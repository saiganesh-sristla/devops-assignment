from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello, World!'

@app.route('/login')
def login():
    return "Successfully logged in"

@app.route('/logout')
def logout():
    return "logged out"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)

